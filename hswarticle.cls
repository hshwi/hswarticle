%
%   LaTeX-Class for Presentations 
%   (e.g. at HSW)
%
%   author(s):  xld (info@xladde.de)

%   license(s):
%   The Source code of this document is provided under the terms of the
%               GNU GENERAL PUBLIC LICENSE v3
%               http://www.gnu.org/licenses/gpl-3.0.html    
%   The Theme options, color combinations and the logos are distributed under the
%   copyright of Wismar International Graduation Services GmbH and Hochschule Wismar
%
\LoadClass[papersize=a4paper, fontsize=12pt]{scrartcl}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{hswthesis}[2017/02/28 Custom class for scientific papers]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   VARIABLES
%
\newcommand{}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   DEPARTMENT AND THEME SPECIFIC CONFIGURATIONS
%
%   The following Options can be configured:
%   1. Departments of HSW (adm, fww, fiw, fg, wings)
\DeclareOption{adm}{\relax}
\DeclareOption{fww}{\relax}
\DeclareOption{fiw}{\relax}
\DeclareOption{fg}{\relax}
\DeclareOption{wings}{\relax}
\DeclareOption*{\relax}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   IMPORTING PACKAGES
%
\ProcessOptions
\relax
% inputencryption
\RequirePackage[utf8]{inputenc}
% fontencryption
\RequirePackage[T1]{fontenc}
% language package
\RequirePackage[ngerman]{babel}
% Image and Graphic support
\RequirePackage{graphicx}
% Using 'libertine' as default font
% There is no LaTeX support for WINGS or HSW meta font\RequirePackage{graphicx}
\RequirePackage{libertine}
% Package for Lorem ipsum ...
\RequirePackage{lipsum}
% Page geometry
\RequirePackage{geometry}
    \geometry{
        inner=3cm,
        outer=3cm,
        top=3cm,
        bottom=3cm
    }
%
% Package provides defining colors
% e.g. \definecolor{mycolor}{RGB}{#R,#G,#B}
%      \color{mycolor}Text colored with mycolor
%
\RequirePackage{xcolor}
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   DEFINING COLORS
    %
    \definecolor{black}{RGB}{0,0,0}
    \definecolor{white}{RGB}{255,255,255}
    \definecolor{wings}{RGB}{0,50,95}
    \definecolor{lwings}{RGB}{100,150,195}
    \definecolor{fiw}{RGB}{0,177,219}
    \definecolor{fww}{RGB}{51,153,51}
    \definecolor{fg}{RGB}{255,93,2}
    \definecolor{dark}{RGB}{47,50,41}
    \definecolor{silver}{RGB}{175,175,175}
    \definecolor{lsilver}{RGB}{240,240,240}
% bibliography
\RequirePackage[numbers]{natbib}
% math symbols etc
\RequirePackage{amsmath}
% math symbols
\RequirePackage{amssymb}
% index generation
\RequirePackage{makeidx}            
% nomenclature settings
\RequirePackage[intoc]{nomencl}     
% Linespacing
\RequirePackage{setspace}           
% floating objects
\RequirePackage{float}              
% caption inside multifloat
\RequirePackage{caption}            
% display sourcecode
\RequirePackage{listings}           
% paragraph columns
\RequirePackage{multicol}           
% generate lipsum
\RequirePackage{lipsum}             
% CC-Icons
\RequirePackage{cclicenses}         
% algorithm formatting
\RequirePackage{algorithm}          
% pseudocode for algorithms
\RequirePackage{algpseudocode}      
% something about font etc..
\RequirePackage{lmodern}            .
% use Libertine fontset
\RequirePackage{libertine}          
% official EUR-Symbol
\RequirePackage{eurosym}            
% custom titlepage
\RequirePackage{titling}            
% indent first paragraph after headline
\RequirePackage{indentfirst}
% import inline PDF (and PDF-Graphics)
\RequirePackage{pdfpages}
% PDF settings
\RequirePackage{hyperref}
    \hypersetup{
        unicode=true, 
        bookmarks=true, 
        bookmarksnumbered=false, 
        bookmarksopen=false, 
        breaklinks=false, 
        pdfborder={0 0 0},  
        backref=false, 
        colorlinks=true, 
        pdftitle={B.Sc.-Thesis Thomas Jonitz (114350)---Wismar, \today}, 
        pdfauthor={Thomas Jonitz}
    }
